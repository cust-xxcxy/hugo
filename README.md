# Tech Stack

- Jenkins
- Gitlab
- Hugo
- Markdown
- Docker

## Directory Structure
- hugo-site - the hugo site template
- Jenkinsfile - the script for Jenkins
- docker - the script for build Jenkins docker and run

## Jenkins setup

- Go to docker folder and run `docker-compose up -d`
- Get jenkins admin's password `docker exec docker_jenkins cat /var/jenkins_home/secrets/initialAdminPassword`

### Integrating Gitlab with jenkins

- Creating Acess Token in gitlab
  - Go to your profile and select [Access Tokens](https://gitlab.com/profile/personal_access_tokens) section
  - Name the access_token
  - Allow scopes
  - Click on create personal access token
    ![screenshot](/screenshots/3.png)
    ![screenshot](/screenshots/4.png)
  - A token will be created
- Configure the Jenkins Server
  - Make sure You installed Jenkins Git and Gitlab plugin
  - Go to Manage Jenkins
  - Configure System
  - Check whether gitlab section is present else you need to **install Jenkin's gitlab plugin**
  - You must enter custom connection name
  - Your Gitlab server URL in Gitlab host URL ![screenshot](/screenshots/5.png)
  - Click on Add Jenkins Credentials section
  - ![screenshot](/screenshots/6.png)
  - Then, fill required fields as shown below with the Jenkins Access API Token which we created in GitLab before.
  - ![screenshot](/screenshots/7.png)
  - Then, select this API Token credential and finally click “Test” and see the “Success” message. -![screenshot](/screenshots/8.png)

### Configuring Environmental variables

You should also add the following environment variables. Which are used in the serverless script to deploy the static website to s3 for cloudfront delivery:

These are required since jenkin's pipeline deploy's static assets to your AWS s3

- AWS_ACCESS_KEY_ID
  - This is your AWS_ACCESS_KEY_ID which you can find in your [AWS console](https://console.aws.amazon.com/)
- AWS_SECRET_ACCESS_KEY
  - This is your AWS_SECRET_ACCESS_KEY which you can find in your [AWS console](https://console.aws.amazon.com/)
- AWS_REGION
  - This is your AWS_REGION which you can find in your [AWS console](https://console.aws.amazon.com/)

To add the variables:

- Go to Credentials -> Jenkins -> Global credentials (unrestricted) -> Add credentials
- On the Kind drop-down select (SECRET_TEXT)
- Add the secret code in the SECRET SECTION and add the name of ID as the variable name
- ![screenshot](/screenshots/9.png)
- Repeat for all 3 variables above

### Configuring Variables in JenkinsFile

Update the DEPLOYMENT_BUCKET in `Jenkins` file to use a unqiue s3 bucket name

- DEPLOYMENT_BUCKET
  - This is the name of your s3 bucket where your static files will be stored and it should be unique
  - In Jenkinsfile go to environment section
  - Manually change the name of DEPLOYMENT_BUCKET to your S3 bucket name

### Create Pipeline

- Click on New Item and select the Pipeline option to create a pipeline
- ![screenshot](/screenshots/10.jpg)
- ![screenshot](/screenshots/11.png)
- Go to Gitlab Connection and select the gitlab connection setup in preparation step
- ![screenshot](/screenshots/12.png)
- In Build Triggers section select Poll SCM option and give this value as schedule: `H * * * *`, which means that jenkin's poll's the repo for every hour
- ![screenshot](/screenshots/13.png)
- Go to pipeline section
  - Select Pipeline script from SCM option
  - SCM as git
  - In repository url give your Gitlab wiki URL
  - In credentials section click on Add and give your Gitlab username and password
  - ![screenshot](/screenshots/14.png)
- Now in Fill the Script Path with the name (Jenkinsfile)
- Click on save and then apply, your pipeline will be saved


### Deploy and Run Pipeline


- Copy the `hugo-site` and `Jenkinsfile` to the wiki repository. 
  - `hugo-site` contains the hugo template files
  - Jenkinsfile contains the build and deployment scripts, and you need to update the DEPLOYMENT_BUCKET in it to use a unqiue s3 bucket name
- Update the wiki files according to [Hugo Guide](./Hugo_Guide.md) to ensure the navigation and links are properly created.
- Push the changes to the wiki repo.

When the pipeline is triggered, it will build the static website from markdown files using hugo, and then deploy it to the s3 for cloudfront delivery. The serverless script will setup the s3 bucket and cloudfront automatically.

After running the pipeline the 1st time, if everything is successful, you should be able to access the website through: `http://${DEPLOYMENT_BUCKET}.s3-website-${AWS_REGION}.amazonaws.com`

Then you can get the Cloudfront URL from your AWS console and access it from the Cloudfront URL.
