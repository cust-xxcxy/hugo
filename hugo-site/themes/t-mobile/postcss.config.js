var autoprefixer = require('autoprefixer');

module.exports = {
    plugins: [
        autoprefixer({ grid: true })
    ],
}