## Navigation

To get side navigation working you must update all the wiki files in your repo using below steps

### Folder-level

Folder level navigation means how to get the subfolder(folder inside a folder) name along with files present in it

**You will get all your root folders present in wiki file's in your navigation by default**

- ### Example Directory structure (initial):

  - Architectures [Folder]
    - Deployment Architecture [Sub Folder]
      - `Architecture.md` [File]

- ### Steps

  - Go inside your sub folder and create an \_index.md file
  - Fill your \_index.md file with the below front-matter
  - Follow [instructions](#front-matter) when adding front-matter

  - ```
     ---
        title: 'deployment-architecture'
     ---
    ```
  - Here title is the sub folder name to get in your navigation

- ### Example Directory structure (final):

  - Architectures [Folder]
    - Deployment Architecture [Sub Folder]
      - `Architecture.md` [File]
      - `\_index.md` [File]

The navigation look's like this (A folder with name deployment-architecure is created in architectures folder)

But you can't see any file in the sub folder created (check by clicking on + button) to get those files follow [File level](#File-level)

![screenshot](/screenshots/1.png)

### File-level

File level navigation means how to get the file's (\*.md) in your navigation

**These steps are same for any (\*.md) file to get in navigation whether that file is present in a folder (or) a subfolder**

- ### Example Directory structure:

  - Architectures [Folder]
    - `Browser-compatibility-guide.md` [File]

- ### Steps

  - Open your file and prepend it with the below front-matter
  - ```
     ---
        title: 'browser-compatibility-guide'
     ---
    ```
  - Follow [instructions](#front-matter) when adding front-matter
  - Here title is the file name to get in your navigation

The navigation look's like this (A file with name browser-comaptibility-guide) is created in folder (Architectures)

![screenshot](/screenshots/2.png)

### front-matter

- When adding required front matter make sure you prepend that to you Markdown content but not append to that


## Links in Markdown file

For images:
- Create a new folder images in wiki repo
- Include all images in this folder (those which are to be refrenced in \*.md files)
- Reference these images in your \*.md files using absolute links (Because relative links cannot work with hugo)

The same applies when you reference other files in the markdown file. 

#### Example to change relative links to absolute links

```
   [image](DW3.0-Full-Stack.jpg) ----> Relative link
   [image](/images/DW3.0-Full-Stack.jpg) ----> Absolute link
```

- Create a new folder assets in wiki repo
- Include all text(_.txt) files and json(_.json) files in this folder (those which are to be refrenced in \*.md files)
- Reference these .json or .txt files in your \*.md files using absolute links (Because relative links cannot work with hugo)

#### Example to change relative links to absolute links

```
   [audit-records.json](audit-records.json) ----> Relative link
   [audit-records.json](/assets/audit-records.json) ----> Absolute link
```
